#!/usr/bin/env python
__author__ = "Nicholas Hill - modified from Python for Microscopists"
__license__ = "Feel free to copy, I appreciate if you acknowledge Python for Microscopists"

# This code has been modified from Sreenivas Bhattiprolu youtube channel called Python for Microscopists.
# This is a valuable resource for any microscopists looking to apply deep learning to their cell segmentation needs.

# https://www.youtube.com/watch?v=0kiroPnV1tM
# https://www.youtube.com/watch?v=cUHPL_dk17E
# https://www.youtube.com/watch?v=RaswBvMnFxk

import tensorflow as tf
import os
import random
import numpy as np

from tqdm import tqdm

from skimage.io import imread, imshow

import matplotlib.pyplot as plt

#%%
IMG_WIDTH = 512
IMG_HEIGHT = 512
IMG_CHANNELS = 1

TRAIN_PATH = '/Volumes/Nick Lab HD/deep learning/pythonDL/separatedImages/trainingData/'
TEST_PATH = '/Volumes/Nick Lab HD/deep learning/pythonDL/separatedImages/testingData/'

# To remove those annoying hidden files starting with '._' Must run in terminal
# dot_clean /Volumes/Nick\ Lab\ HD/deep\ learning/pythonDL/separatedImages/testingData/

train_imgids = next(os.walk(TRAIN_PATH + 'images/'))[2]
train_maskids = next(os.walk(TRAIN_PATH + 'masks/'))[2]

X_train = np.zeros((len(train_imgids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint16)
Y_train = np.zeros((len(train_maskids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool)
print('Loading in training images')
for n, id_ in tqdm(enumerate(train_imgids), total=len(train_imgids)):
    imgpath = TRAIN_PATH + 'images/' + id_
    img = imread(imgpath)[:,:]
    img = np.expand_dims(img, 2)
    X_train[n] = img  #Fill empty X_train with values from img
    
#Now do the same for the masks
print('Loading in training masks')
for n, id_ in tqdm(enumerate(train_maskids), total=len(train_maskids)):
    maskpath = TRAIN_PATH + 'masks/' + id_
    mask = imread(maskpath)[:,:]
    mask = np.expand_dims(mask, 2)
    Y_train[n] = mask  #Fill empty Y_train with values from mask


# And now, do the same for test images - do not load in these masks
test_imgids = next(os.walk(TEST_PATH + 'images/'))[2]

X_test = np.zeros((len(test_imgids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint16)
print('Loading in test images')
for n, id_ in tqdm(enumerate(test_imgids), total=len(test_imgids)):
    imgpath = TEST_PATH + 'images/' + id_
    img = imread(imgpath)[:,:]
    img = np.expand_dims(img, 2)
    X_test[n] = img  #Fill empty X_train with values from img

print('Done loading in images and masks!')

# Test to make sure images and masks look as expected
image_x = random.randint(0, len(train_imgids))
imshow(X_train[image_x][:,:,0])
plt.show()
imshow(np.squeeze(Y_train[image_x][:,:,0]))
plt.show()



#%%
#Build the model
inputs = tf.keras.layers.Input((IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS))
s = tf.keras.layers.Lambda(lambda x: x / 65535)(inputs)

#Contracting path
c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(s)
c1 = tf.keras.layers.Dropout(0.1)(c1)
c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c1)
p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p1)
c2 = tf.keras.layers.Dropout(0.1)(c2)
c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c2)
p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p2)
c3 = tf.keras.layers.Dropout(0.2)(c3)
c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c3)
p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p3)
c4 = tf.keras.layers.Dropout(0.2)(c4)
c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c4)
p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p4)
c5 = tf.keras.layers.Dropout(0.3)(c5)
c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)

#Expanding path
u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c5)
u6 = tf.keras.layers.concatenate([u6, c4])
c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
c6 = tf.keras.layers.Dropout(0.2)(c6)
c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)

u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6)
u7 = tf.keras.layers.concatenate([u7, c3])
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u7)
c7 = tf.keras.layers.Dropout(0.2)(c7)
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c7)

u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c7)
u8 = tf.keras.layers.concatenate([u8, c2])
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u8)
c8 = tf.keras.layers.Dropout(0.1)(c8)
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c8)

u9 = tf.keras.layers.Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(c8)
u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u9)
c9 = tf.keras.layers.Dropout(0.1)(c9)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c9)

outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

model = tf.keras.Model(inputs=[inputs], outputs=[outputs])
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model.summary()
#%%

#Modelcheckpoint
checkpointer = tf.keras.callbacks.ModelCheckpoint('/Volumes/Nick Lab HD/deep learning/pythonDL/model_for_cyano.h5', verbose=1, save_best_only=True)

callbacks = [
        tf.keras.callbacks.EarlyStopping(patience=2, monitor='val_loss'),
        tf.keras.callbacks.TensorBoard(log_dir='logs')]

results = model.fit(X_train, Y_train, validation_split=0.1, batch_size=16, epochs=100, callbacks=callbacks)

####################################

preds_train = model.predict(X_train[:int(X_train.shape[0]*0.9)], verbose=1)
preds_val = model.predict(X_train[int(X_train.shape[0]*0.9):], verbose=1)
preds_test = model.predict(X_test, verbose=1)

preds_train_t = (preds_train > 0.5).astype(np.uint16)
preds_val_t = (preds_val > 0.5).astype(np.uint16)
preds_test_t = (preds_test > 0.5).astype(np.uint16)


# Perform a sanity check on some random training samples
ix = random.randint(0, len(preds_train_t))
imshow(X_train[ix][:,:,0])
plt.show()
imshow(np.squeeze(Y_train[ix][:,:,0]))
plt.show()
imshow(np.squeeze(preds_train_t[ix][:,:,0]))
plt.show()

# Perform a sanity check on some random validation samples
ix = random.randint(0, len(preds_val_t))
imshow(X_train[int(X_train.shape[0]*0.9):][ix][:,:,0])
plt.show()
imshow(np.squeeze(Y_train[int(Y_train.shape[0]*0.9):][ix][:,:,0]))
plt.show()
imshow(np.squeeze(preds_val_t[ix][:,:,0]))
plt.show()


# To view tensorboard graphs
# !tensorboard --logdir=logs/ --host localhost --port 8088

#%% To save and load trained net

# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")

# later...
 
# load json and create model
json_file = open('/Volumes/Nick Lab HD/deep learning/pythonDL/LargerDataSet/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = tf.keras.models.model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("/Volumes/Nick Lab HD/deep learning/pythonDL/LargerDataSet/model.h5")
print("Loaded model from disk")

#%%

#Test the loaded model
newImgPath = '/Volumes/Nick Lab HD/deep learning/pythonDL/furtherTestImages/smallerImg.tif'
img = imread(newImgPath)
imshow(img)
plt.show()
img = np.expand_dims(img, 0)
img = np.expand_dims(img, 3)
predImg = loaded_model.predict(img, verbose=1)
predImg = (predImg > 0.5).astype(np.uint16)
imshow(predImg[0][:,:,0])
plt.show()



