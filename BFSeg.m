function redSegNCHExample(imgPath, varargin)
%REDSEG segments images of bacterial cells taken in the brightfield channel
%on a widefield microscope.
%   REDSEG(imgPath) segments cells in the image found at imgPath, and
%   returns a mask of the image as a .tif.
%   Segmentation parameters can be optionally defined. Default parameters
%   work well for Synechococcus sp. PCC 7002 cells.
%
%           Written by Nicholas Hill

%Settings
opts = inputParser;
addParameter(opts, 'ThresholdLevel', 10);
addParameter(opts, 'MaxCellMinDepth', 5);
addParameter(opts, 'OutlierThLvl', 60);
addParameter(opts, 'MaxCellArea', 3500);
opts.parse(varargin{:});

%Load in the image
cellImage = imread(imgPath);

%Make histogram of pixel intensity values. Visualize with plot(binCenters, nCnts)
[nCnts, binEdges] = histcounts(cellImage(:),linspace(0, double(max(cellImage(:))), 150));
binCenters = diff(binEdges) + binEdges(1:end-1);

%Find the peak background
[bgPk, bgPkLoc] = max(nCnts);

%Find threshold level.
thLoc = bgPkLoc - opts.Results.ThresholdLevel;
thLvl = binCenters(thLoc);

%compute mask. Dark pixels (below ThresholdLevel) are considered cells
mask = cellImage < thLvl;
%mask = imcomplement(mask);
mask = imfill(mask, 'holes');

%Separate cell clumps using watershedding
dd = -bwdist(~mask);
dd(~mask) = -Inf;
dd = imhmin(dd, opts.Results.MaxCellMinDepth);
LL = watershed(dd);
mask(LL == 0) = 0;

%Tidy, and thicken
mask = bwareaopen(mask, 100);
mask = bwmorph(mask, 'thicken', 8);

%Identify outlier (large) objects and try to split them
rpCells = regionprops(mask, {'Area','PixelIdxList'});
outlierCells = find([rpCells.Area] > opts.Results.MaxCellArea);


for iCell = outlierCells
    
    %Perform local, more stringent, thresholding to break up cell clumps
    %that escaped initial watershedding
    currMask = false(size(mask));
    currMask(rpCells(iCell).PixelIdxList) = true;
    thLvl = prctile(cellImage(currMask), opts.Results.OutlierThLvl); %Increase # to keep more white pixels
    
    newMask = cellImage < thLvl;
    newMask(~currMask) = 0;
    
    newMask = imfill(newMask, 'holes');
    
    %Perform more stringent watershedding
    dd = -bwdist(~newMask);
    dd(~newMask) = -Inf;
    dd = imhmin(dd, opts.Results.MaxCellMinDepth-2);
    LL = watershed(dd);
    newMask(LL == 0) = 0;
    
    %Tidy, and thicken
    newMask = bwareaopen(newMask, 100);
    newMask = bwmorph(newMask,'thicken', 8);
    
    %Replace the old masks
    mask(currMask) = 0;
    mask(currMask) = newMask(currMask);
end

imwrite(mask, 'Mask.tif', 'Compression', 'None');

end
